<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenchmarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         Schema::create('benchmarks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('economic_sector_code');
            $table->integer('segment_value');
            $table->integer('observations');
            $table->double('avg_rsv',28,14);
            $table->double('stddev_rsv',28,14);
            $table->double('max_rsv',28,14);
            $table->double('min_rsv',28,14);
            $table->double('avg_iremven',28,14);
            $table->double('stddev_iremven',28,14);
            $table->double('max_iremven',28,14);
            $table->double('min_iremven',28,14);
            $table->double('avg_roi',28,14);
            $table->double('stddev_roi',28,14);
            $table->double('max_roi',28,14);
            $table->double('min_roi',28,14);
            $table->double('avg_apa',28,14);
            $table->double('stddev_apa',28,14);
            $table->double('max_apa',28,14);
            $table->double('min_apa',28,14);
            $table->double('avg_remunerations_vs_go',28,14);            
            $table->double('stddev_remunerations_vs_go',28,14);
            $table->double('max_remunerations_vs_go',28,14);
            $table->double('min_remunerations_vs_go',28,14);
            $table->double('avg_sells',28,14);
            $table->double('stddev_sells',28,14);
            $table->double('max_sells',28,14);
            $table->double('min_sells',28,14);
            $table->double('avg_remunerations',28,14);
            $table->double('stddev_remunerations',28,14);
            $table->double('max_remunerations',28,14);
            $table->double('min_remunerations',28,14);
            $table->double('avg_assets',28,14);
            $table->double('stddev_assets',28,14);
            $table->double('max_assets',28,14);
            $table->double('min_assets',28,14);
            $table->double('avg_workers',28,14);
            $table->double('stddev_workers',28,14);
            $table->double('max_workers',28,14);
            $table->double('min_workers',28,14);
            $table->double('max_sells_3',28,14);
            $table->double('max_assets_3',28,14);           

        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('benchmarks');
    }
}
