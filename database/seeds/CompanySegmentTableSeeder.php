
<?php

use Illuminate\Database\Seeder;

class CompanySegmentTableSeeder extends Seeder
{
    
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cmmd = "mysql --default-character-set=utf8 -u ".env('DB_USERNAME')." -h ".env('DB_HOST')." -p".env('DB_PASSWORD')." ".env('DB_DATABASE')." < ".__DIR__."/sql/Company_Segment.sql 2>&1 | grep -v 'Warning: Using a password'";

        exec($cmmd, $out, $err);
    }
}
