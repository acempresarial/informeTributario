<div class="col-md-6">   
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">BenchMark: Eficiencia en las Remuneraciones</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                        
                <bench-mark-graph 
                  :minimum="{{$report->data->analysis->Benchmark->Productivity->min}}"
                  :maximum="{{$report->data->analysis->Benchmark->Productivity->max}}"
                  :average="{{$report->data->analysis->Benchmark->Productivity->avg}}"
                  :company="{{$report->data->analysis->Benchmark->Productivity->company}}">
                </bench-mark-graph>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>  
