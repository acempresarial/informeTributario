<div class="col-md-6">   
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Remuneraciones v/s Ventas para los últimos periodos</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">             
              <remunerations-sells-graph
              :labels="{{$report->data->analysis->Business->Charts->salesLast3Years->labels}}"
              :remunerations="{{$report->data->analysis->HHRR->Charts->RemunerationsLast3Years->remunerations}}"
              :sells="{{$report->data->analysis->Business->Charts->salesLast3Years->sales}}">
              </remunerations-sells-graph>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>  
