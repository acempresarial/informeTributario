 <div class="panel box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordionRemunerations" href="#collapseOneRemunerations" class="collapsed" aria-expanded="false">
          Remuneraciones Anuales (Últimos 3 años)
        </a>
      </h4>
    </div>
    <div id="collapseOneRemunerations" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
      <div class="box-body">     

	
      @forelse($report->data->analysis->HHRR->Remunerations->Pesos as $key => $remuneration)
        En el año <strong>{{$remuneration->year}} </strong> se pagaron <strong>MM$ {{$remuneration->millions_amount}}</strong> en remuneraciones.       
        	@if(isset($report->data->analysis->HHRR->Remunerations->Pesos[$key + 1]))
        	Las remuneraciones pagadas durante el año {{$remuneration->year}} <strong>{{$remuneration->situation}}</strong> en un <strong>{{abs($remuneration->porcentage)}}%</strong> con respecto al año       	 {{$report->data->analysis->HHRR->Remunerations->Pesos[$key + 1]->year}} (MM$ {{$report->data->analysis->HHRR->Remunerations->Pesos[$key + 1]->millions_amount}})
        	@endif
       
        <br>
        <br>
      @empty
      @endforelse
    
      </div>
    </div>
  </div>