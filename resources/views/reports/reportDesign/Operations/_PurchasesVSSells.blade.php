<div class="col-md-6">   
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Comparación Ventas v/s Compras Últimos 12 meses</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">
              <div class="chart">             

                <sells-purchases-comparison-graph
                  :labels="{{$report->data->analysis->Operations->Charts->PurchasesLast12Month->labels}}"
                  :purchases="{{$report->data->analysis->Operations->Charts->PurchasesLast12Month->purchases}}"
                  :sales="{{$report->data->analysis->Business->Charts->salesLast12Month->sales}}">
                </sells-purchases-comparison-graph>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>
