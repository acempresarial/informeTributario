<div class="col-md-6">   
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Diferencia entre Ventas y Compras Últimos 12 meses</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">
              <div class="chart">            
                <sells-purchases-diff-graph
                :labels="{{$report->data->analysis->Operations->Charts->PurchasesLast12Month->labels}}"
                :difference="{{$report->data->analysis->Operations->Charts->PurchasesSellsDifference->difference}}"
                :upper="{{$report->data->analysis->Operations->UpperLimit}}"
                :lower="{{$report->data->analysis->Operations->LowerLimit}}"
                :avg="{{$report->data->analysis->Operations->AvgDifference}}"
                >
                </sells-purchases-diff-graph>

              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>
