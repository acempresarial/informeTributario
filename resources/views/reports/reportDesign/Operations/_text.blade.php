<div class="col-md-12">   
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Explicación del Informe</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
          <div class="box box-solid">
                       <div class="box-body">
              <div class="box-group" id="OperationsAcordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                @include('reports/reportDesign/Operations/BenchMarktexts/_last12Months')
                @include('reports/reportDesign/Operations/BenchMarktexts/_SellsandPurchasesDifferences')              
                
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>