<div class="col-md-12">   
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Compras Últimos 12 meses</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">            

               <last-12-months-graph 
                  :labels="{{$report->data->analysis->Operations->Charts->PurchasesLast12Month->labels}}"
                  :values="{{$report->data->analysis->Operations->Charts->PurchasesLast12Month->purchases}}" 
                  :valuelabel="'Compras en $MM'"
                  :porcentages="{{$report->data->analysis->Operations->Charts->PurchasesLast12Month->porcentages}}"
                  :porcentageslabel="'% del Total'">                    
                </last-12-months-graph>               

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>
