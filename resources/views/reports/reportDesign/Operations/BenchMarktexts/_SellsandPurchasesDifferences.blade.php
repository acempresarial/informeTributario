 <div class="panel box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#OperationsAcordion" href="#collapseTwoOP" class="collapsed" aria-expanded="false">
          Diferencias entre compras y ventas
        </a>
      </h4>
    </div>
    <div id="collapseTwoOP" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
      <div class="box-body">
        Normalmente el exceso de inventarios es un costo innecesario para las empresas. El promedio de diferencias entre ventas y compras mensuales es de <strong>MM$ {{$report->data->analysis->Operations->AvgDifference}}</strong>. El mes en que las compras son similares a las ventas es <strong>{{$report->data->analysis->Operations->MonthMinDifference->strings->month}}</strong>.
        <!--
         ¿Es posible en tu rubro reducir ese promedio de stock?
        ¿Es correcto para su rubro que se haya producido un sobrestock de materias primas en el mes de octubre de 2010? ¿No sería conveniente hacer esfuerzos por anticiparse mejor a las ventas al momento de comprar?
        -->
      </div>
    </div>
  </div>