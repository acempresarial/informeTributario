<div class="panel box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
          Ventas Últimos 12 Meses
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
      <div class="box-body">
       <p align="justify">
       Las ventas totales de la empresa <strong> {{$report->data->analysis->GeneralInformation->CompanyName}} </strong> en el último periodo de 12 meses fueron <strong> MM$ {{number_format($report->data->analysis->Business->SalesLastYear->amount / 1000000 ,2,',','.')}}</strong>.
       </p>
      <p align="justify">

       En los últimos 12 meses corridos, las mayores ventas, por
        <strong> MM$ {{$report->data->analysis->Business->MaxSale->formatted->millions}}</strong>, se produjeron en el mes de 
        <strong>{{$report->data->analysis->Business->MonthMaxSale}}</strong>, esto equivale al 
        <strong> {{$report->data->analysis->Business->PercentageMaxSale}}%</strong> 
        de las ventas totales de los 12 últimos meses. En tanto, el mes de menor venta por
         <strong>MM$ {{$report->data->analysis->Business->MinSale->formatted->millions}}</strong>, fue 
         <strong>{{$report->data->analysis->Business->MonthMinSale}}</strong>, lo cual equivale al 
         <strong> {{$report->data->analysis->Business->PercentageMinSale}}%</strong> 
          de las ventas totales de este periodo. 

          <!--¿Es correcto para tu rubro que haya una gran diferencia en ventas entre los meses de julio de 2011 y octubre de 2010 o sería conveniente hacer esfuerzos por aumentar las ventas en el mes octubre de 2010?-->
       </p>

              
      </div>
    </div>
</div>