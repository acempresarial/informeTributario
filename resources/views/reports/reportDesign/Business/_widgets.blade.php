<div class="row">

      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-usd"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Ventas Totales</span>
              <span class="info-box-number">{{number_format($report->data->analysis->Business->TotalSales,0,'.','.')}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
                  <span class="progress-description">
                     Ventas últimos 12 Meses
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
      </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
           <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">MES MAYOR VENTAS</span>
              <span class="info-box-number">{{$report->data->analysis->Business->MonthMaxSale}}
                (MM$ {{$report->data->analysis->Business->MaxSale->formatted->millions}})</span>

              <div class="progress">
                <div class="progress-bar" style="width:0%"></div>
              </div>
                  <span class="progress-description">
                    {{$report->data->analysis->Business->PercentageMaxSale}}% Ventas Anuales
                  </span>
            </div>


            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
       

        

         <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">MES MENOR VENTAS</span>
              <span class="info-box-number">{{$report->data->analysis->Business->MonthMinSale}}
                (MM$ {{$report->data->analysis->Business->MinSale->formatted->millions}})</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    {{$report->data->analysis->Business->PercentageMinSale}}% Ventas Anuales
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

    
      </div>