<div class="col-md-4">   
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Total de Activos</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">
              <div class="chart">            

                 <last-3-years-graph 
                  :labels="{{$report->data->analysis->Financial->Charts->TotalAssets->labels}}"
                  :values="{{$report->data->analysis->Financial->Charts->TotalAssets->TotalAssets}}" 
                  :legend="'Total Activos $MM'"
                  >                    
                </last-3-years-graph>  
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>  
