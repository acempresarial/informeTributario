<div class="col-md-12">
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Indicadores Financieros</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
              <div class="table-responsive">
                <table class="table no-margin">
                  <th class="col-md-3">Indicadores Financieros de la empresa</th>
                  <th class="col-md-3"></th>
            
                  @forelse($report->data->analysis->Financial->FinancialIndicators->ReturnsOnSales as $return)
                  <th class="col-md-2" style="text-align: right">{{$return->year}}</th>
                  @empty
                  @endforelse
                  
                  <tbody>
                  <tr >
                    <td style="text-align: right"> </td>
                    <td style="text-align: left">Retorno sobre las Ventas</td>
                     @forelse($report->data->analysis->Financial->FinancialIndicators->ReturnsOnSales as $return)
                    <td style="text-align: right"> {{$return->amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                  <tr >
                    <td style="text-align: right"> </td>
                    <td style="text-align: left">Retorno sobre los Activos</td>                    
                    @forelse($report->data->analysis->Financial->FinancialIndicators->ReturnsOnAssets as $return)
                      <td style="text-align: right"> {{$return->amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                   <tr >
                   <td style="text-align: right"><strong></strong></td>
                    <td style="text-align: left">Rentabilidad sobre el Patrimonio</td>                 
                    @forelse($report->data->analysis->Financial->FinancialIndicators->ProfitOverEquity as $return)
                      <td style="text-align: right">{{$return->amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                  <tr >
                   <td style="text-align: right"></td>
                    <td style="text-align: left">Rotación de Activos</td>                    
                    @forelse($report->data->analysis->Financial->FinancialIndicators->AssetsRotation as $rotation)
                      <td style="text-align: right">{{$rotation->value}}</td>
                    @empty
                    @endforelse
                  </tr>                    
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix" style="display: block;">
           
            </div>
            <!-- /.box-footer -->
          </div>
          </div>