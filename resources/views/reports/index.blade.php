@extends('layouts.app')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    
    <h1>
    Reportes Generados
    <small>Reportes históricos generados para esta Carpeta</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Carpetas Tributarias</a></li>
         <li><a href="#">Reportes</a></li>
        <li class="active">Subir</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
      
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Reportes Generados</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="reports" class="table table-bordered table-striped dataTable">
                        <thead>
                            <th>Empresa</th>
                            <th>Fecha Generación de la Carpeta</th>
                                                       
                            <th>Ver Informe</th>
                            
                        </thead>
                        
                        <tbody>
                            @forelse($reports as $report)
                            <tr>
                                <td>{{$report->name}}</td>
                                <td>{{$report->created_at}}</td>                               
                              
                                <td>
                                    <a href="/reports/{{$report->id}}" type="button" class="btn btn-block btn-primary btn-flat">
                                        <i class="fa fa-list-alt fa-5" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                            
                            @empty
                            frefrefre
                            @endforelse
                        </tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="/cte" class="btn btn-default">Volver</a>                
                 </div>
            </div>
            
        </section>
        <!-- /.content -->
        @endsection
        @section('footer_scripts')
        <script type="text/javascript">
        $(document).ready(function(){
        $('#reports').DataTable({
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ resultados",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando _START_ al _END_ de un total de _TOTAL_ resultados",
        "sInfoEmpty":      "Sin resultados",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ resultados)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
        },
        "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
        });
        });
        </script>
        @endsection