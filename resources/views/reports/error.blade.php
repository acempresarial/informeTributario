@extends('layouts.app')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    
    <h1>
    Reporte: 
    <small>Reportes generados para esta Carpeta</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Carpetas Tributarias</a></li>
         <li><a href="#">Reporte</a></li>
        <li class="active">Subir</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">

    <div class="col-md-12">
       Su carpeta tributaria no pudo ser procesada por los siguientes motivos:
  <ul class="list-group">
       @forelse($errors['messages'] as $error)
         @if(isset($error['state']))
            <li class="list-group-item list-group-item-danger"> {{$error['reason']}}</li>
         @endif        
       @empty
       @endforelse
  </ul>
    </div>
        
   
            
            
</section>
        <!-- /.content -->
@endsection
     