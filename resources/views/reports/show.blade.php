@extends('layouts.app')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    
    <h1>
    Reporte: {{$report->name}}
    <small>Reportes generados para esta Carpeta</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Carpetas Tributarias</a></li>
         <li><a href="#">Reporte</a></li>
        <li class="active">Subir</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">

    <div class="col-md-12">
          <!-- Custom Tabs (Pulled to the right) -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right"> 
            <!--     
            <li class=""><a href="#Conclusions" data-toggle="tab" aria-expanded="false">VIII. CONCLUSIONES</a></li>-->
             <li class=""><a href="#TaxBenefits" data-toggle="tab" aria-expanded="false">VI. BENEFICIOS TRIBUTARIOS</a></li>
             <!--
             <li class=""><a href="#Competitiveness" data-toggle="tab" aria-expanded="false">VI. COMPETITIVIDAD</a></li>-->
             <li class=""><a href="#Finances" data-toggle="tab" aria-expanded="false">V. FINANZAS</a></li>
            <li class=""><a href="#Remunetarions" data-toggle="tab" aria-expanded="false">IV. REMUNERACIONES</a></li>
              <li class=""><a href="#Operations" data-toggle="tab" aria-expanded="false">III. OPERACIONES</a></li>
              <li class=""><a href="#Business" data-toggle="tab" aria-expanded="false">II.COMERCIAL</a></li>
              <li class="active"><a href="#Scopes" data-toggle="tab" aria-expanded="true">I. RESUMEN</a></li>
              <!--
               <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-gear">Acciones</i> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Exportar a PDF</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li>
              -->
              <li class="pull-left header"><i class="fa fa-th"></i>Informe Tributario</li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="Scopes">
                    @include('reports/reportDesign/_scope')
                </div>
                <div class="tab-pane" id="Business">
                    @include('reports/reportDesign/_business')
                </div>
                <div class="tab-pane" id="Operations">
                    @include('reports/reportDesign/_operations')
                </div>
                <div class="tab-pane" id="Remunetarions">
                   @include('reports/reportDesign/_remunerations')
                </div>
                <div class="tab-pane" id="Finances">
                  @include('reports/reportDesign/_finances')
                </div>
                <div class="tab-pane" id="Competitiveness">
                  @include('reports/reportDesign/_competitiveness')
                </div>
                <div class="tab-pane" id="TaxBenefits">
                   @include('reports/reportDesign/_taxBenefits')
                </div>
                <div class="tab-pane" id="Conclusions">
                   @include('reports/reportDesign/_conclusions')
                </div>
             
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        
   
            
            
</section>
        <!-- /.content -->
        @endsection
        @section('footer_scripts')
        <script src="/js/main.js"></script>
       
       
        
        
        

        @endsection