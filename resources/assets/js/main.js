import Vue from 'vue';
import Last12MonthsGraph from './components/graphs/Last12MonthsGraph';
import Last3YearsGraph from './components/graphs/Last3YearsGraph';
import BenchMarkGraph from './components/graphs/BenchMarkGraph';
import SellsPurchasesComparisonGraph from './components/graphs/SellsPurchasesComparisonGraph';
import SellsPurchasesDiffGraph from './components/graphs/SellsPurchasesDiffGraph';
import RemunerationsSellsGraph from './components/graphs/RemunerationsSellsGraph';
import CompetitivenessGraph from './components/graphs/CompetitivenessGraph';

new Vue({
	el: 'body',
	components: {
		Last12MonthsGraph,
		Last3YearsGraph,
		BenchMarkGraph,
		SellsPurchasesComparisonGraph,
		SellsPurchasesDiffGraph,
		RemunerationsSellsGraph
		CompetitivenessGraph
	}

})


