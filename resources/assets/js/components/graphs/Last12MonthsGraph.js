import Chart from 'chart.js';

export default
{
	template: `
		<div>
			<canvas v-el:canvas style="height: 130px; width: 635px;" width="635" height="230"></canvas>			
		</div>
		`,

	props: ['labels','values','porcentages','valuelabel','porcentageslabel'],	

	ready(){
		let data = {
            labels: this.labels,
            datasets: [  {
                          type: 'bar',
                          label: this.valuelabel,
                          data: this.values,
                          backgroundColor: 'rgba(75, 192, 192, 0.2)',
                          borderColor: 'rgba(75, 192, 192, 1)',
                          hoverBackgroundColor: "rgba(255,99,132,0.4)",
                          hoverBorderColor: "rgba(255,99,132,1)",
                          borderWidth: 1,
                          yAxisID: 'y-axis-1'
                      }, {
                label: this.porcentageslabel,
                    type:'line',
                    data: this.porcentages,
                    fill: false,
                    borderColor: '#EC932F',
                    backgroundColor: '#EC932F',
                    pointBorderColor: '#EC932F',
                    pointBackgroundColor: '#EC932F',
                    pointHoverBackgroundColor: '#EC932F',
                    pointHoverBorderColor: '#EC932F',
                    yAxisID: 'y-axis-2'
            } ]
        };
		
		let myBarChart = new Chart(
			this.$els.canvas.getContext('2d'), 
		{
                type: 'bar',
                data: data,
                options: {
                responsive: true,
                tooltips: {
                  mode: 'label'
              },
              elements: {
                line: {
                    fill: true
                }
            },
              scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    labels: {
                        show: true,
                    }
                }],
                yAxes: [{
                    type: "linear",
                    display: true,
                    position: "left",
                    id: "y-axis-1",
                    gridLines:{
                        display: false
                    },
                    labels: {
                        show:true,
                        
                    }
                }, {
                    type: "linear",
                    display: true,
                    position: "right",
                    id: "y-axis-2",
                    gridLines:{
                        display: false
                    },
                    labels: {
                        show:true,
                        
                    }
                }]
            }
            }
            }


		);

	}

}















