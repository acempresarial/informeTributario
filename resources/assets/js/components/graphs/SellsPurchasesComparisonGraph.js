import Chart from 'chart.js';

export default
{
	template: `
		<div>
			<canvas v-el:canvas style="height: 130px; width: 635px;" width="635" height="300"></canvas>			
		</div>
		`,

	props: ['labels','purchases','sales'],	

	ready(){
		let data = {
            labels: this.labels,
            datasets: [  {
                          type: 'line',
                          label: 'COMPRAS EN MM$',
                          data: this.purchases,
                          backgroundColor: 'rgba(75, 192, 192, 0.2)',
                          borderColor: 'rgba(75, 192, 192, 1)',
                          hoverBackgroundColor: "rgba(255,99,132,0.4)",
                          hoverBorderColor: "rgba(255,99,132,1)",
                          borderWidth: 1,
                          yAxisID: 'y-axis-3',
                          pointRadius:8,
                          pointHoverRadius: 15

                      }, {
                label: "VENTAS EN MM$$",
                    type:'line',
                    data: this.sales,
                    fill: false,
                    borderColor: '#EC932F',
                    backgroundColor: '#EC932F',
                    pointBorderColor: '#EC932F',
                    pointBackgroundColor: '#EC932F',
                    pointHoverBackgroundColor: '#EC932F',
                    pointHoverBorderColor: '#EC932F',
                    pointRadius:8,
                    pointHoverRadius: 15,
                   yAxisID: 'y-axis-3'
            } ]
        };
		
		let myBarChart = new Chart(
			this.$els.canvas.getContext('2d'), 
		{
                type: 'linear',
                data: data,
                options: {
                responsive: true,
                tooltips: {
                  mode: 'label'
              },
              elements: {
                line: {
                    fill: true
                }
            },
              scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    labels: {
                        show: true,
                    }
                }],
                yAxes: [{
                    type: "linear",
                    display: true,
                    position: "left",
                    id: "y-axis-3",
                    gridLines:{
                        display: false
                    },
                    labels: {
                        show:true,
                        
                    }
                }]
            }
            }

            }


		);

	}

}
