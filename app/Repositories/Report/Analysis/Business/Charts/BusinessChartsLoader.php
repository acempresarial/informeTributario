<?php 

namespace acempresarial\Repositories\Report\Analysis\Business\Charts;

use acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast12Month\salesLast12MonthLoader;
use acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast3Years\salesLast3YearsLoader;

/**
* 
*/
class BusinessChartsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Business Charts section
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($CTE,$analysis)
	{		
		$charts = array();		
		$charts['salesLast12Month'] = (new salesLast12MonthLoader)->get($analysis);
		$charts['salesLast3Years'] = (new salesLast3YearsLoader)->get($analysis);

		
		return $charts;
	}
}