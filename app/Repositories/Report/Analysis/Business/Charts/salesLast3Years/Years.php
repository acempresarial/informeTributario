<?php 
namespace acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast3Years;

use acempresarial\Helpers\PHPhelpers;

class Years
{
	private $analysis;
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	

		$helper = new PHPhelpers();
		$result = [];

		$years = $this->analysis['Last3Years'];
		
		foreach ($years as $year) {
			
			$result[] = $helper->datetime_to_string($year)['numbers']['year'];

		}

		$result = array_reverse($result );
		
		$result = collect($result)->toJson();
	
	    return $result;
	}
	
}