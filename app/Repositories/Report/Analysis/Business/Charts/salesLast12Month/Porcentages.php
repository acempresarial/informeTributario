<?php 
namespace acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast12Month;
use acempresarial\Helpers\PHPhelpers;

class Porcentages
{
	private $analysis;
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	/**
	 * [recipe description]
	 * @return [type] [description]
	 */
	private function recipe()
	{	
		$helper = new PHPhelpers();
		$result = [];
	
		$porcentages = $this->analysis['SalesPercentage'];
			
		foreach ($porcentages as $porcentage => $value) {
			array_push($result,$helper->chart_porcentage_formatter($value));
		}	

		$result = array_reverse($result );		
		
		$result = collect($result)->toJson();

	    return $result;
	}
	
}