<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
class Last3Years
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $years = $CTE->f22s;
        $arreglo = [];
        $i=0;
        foreach ($years as $year) {
            $arreglo[$i] = $year->tax_year;
            $i++;
        }
        $result = $arreglo;
        return $result;
    }
}
