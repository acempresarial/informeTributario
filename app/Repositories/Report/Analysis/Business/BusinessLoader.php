<?php 

namespace acempresarial\Repositories\Report\Analysis\Business;

use acempresarial\Repositories\Report\Analysis\Business\Last12MonthsArray;
use acempresarial\Repositories\Report\Analysis\Business\SalesLast12MonthsArray;
use acempresarial\Repositories\Report\Analysis\Business\TotalSalesLast12Months;
use acempresarial\Repositories\Report\Analysis\Business\MonthlySalesUf;
use acempresarial\Repositories\Report\Analysis\Business\TotalMensualSalesUf;
use acempresarial\Repositories\Report\Analysis\Business\SalesPercentage;
use acempresarial\Repositories\Report\Analysis\Business\MaxSale;
use acempresarial\Repositories\Report\Analysis\Business\MinSale;
use acempresarial\Repositories\Report\Analysis\Business\MonthMaxSale;
use acempresarial\Repositories\Report\Analysis\Business\MonthMinSale;
use acempresarial\Repositories\Report\Analysis\Business\PercentageMaxSale;
use acempresarial\Repositories\Report\Analysis\Business\PercentageMinSale;
use acempresarial\Repositories\Report\Analysis\Business\AnnualSales;
use acempresarial\Repositories\Report\Analysis\Business\PercentageAnnualSales;
use acempresarial\Repositories\Report\Analysis\Business\Last3Years;
use acempresarial\Repositories\Report\Analysis\Business\LastYear;
use acempresarial\Repositories\Report\Analysis\Business\SecondLastYear;
use acempresarial\Repositories\Report\Analysis\Business\ThirdLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesUfLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesSecondLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesUfSecondLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesThirdLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesUfThirdLastYear;
use acempresarial\Repositories\Report\Analysis\Business\LastYearSalesVsSecondLastYearSales;
use acempresarial\Repositories\Report\Analysis\Business\SecondLastYearSalesVsThirdLastYearSales;
use acempresarial\Repositories\Report\Analysis\Business\Charts\BusinessChartsLoader;
/**
* 
*/
class BusinessLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the General information page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($CTE)
	{

		$analysis = array();
		$analysis['MonthsArray'] = (new Last12MonthsArray)->get($CTE);
		$analysis['SalesMonths'] = (new SalesLast12MonthsArray)->get($CTE);
		$analysis['TotalSales'] = (new TotalSalesLast12Months)->get($CTE);
		$analysis['MonthlySalesUf'] = (new MonthlySalesUf)->get($CTE);
		$analysis['TotalMensualSalesUf'] = (new TotalMensualSalesUf)->get($CTE);
		$analysis['SalesPercentage'] = (new SalesPercentage)->get($CTE);
		$analysis['MaxSale']= (new MaxSale)->get($CTE);
		$analysis['MinSale']= (new MinSale)->get($CTE);
		$analysis['MonthMinSale']= (new MonthMinSale)->get($CTE);
		$analysis['MonthMaxSale']= (new MonthMaxSale)->get($CTE);
		$analysis['PercentageMinSale']= (new PercentageMinSale)->get($CTE);
		$analysis['PercentageMaxSale']= (new PercentageMaxSale)->get($CTE);
		$analysis['AnnualSales']= (new AnnualSales)->get($CTE);
		$analysis['PercentageAnnualSales']= (new PercentageAnnualSales)->get($CTE);
		$analysis['Last3Years']= (new Last3Years)->get($CTE);
		$analysis['LastYear']= (new LastYear)->get($CTE);
		$analysis['SecondLastYear']= (new SecondLastYear)->get($CTE);
		$analysis['ThirdLastYear']= (new ThirdLastYear)->get($CTE);
		$analysis['SalesLastYear']= (new SalesLastYear)->get($CTE);
		$analysis['SalesUfLastYear']= (new SalesUfLastYear)->get($CTE);
		$analysis['SalesSecondLastYear']= (new SalesSecondLastYear)->get($CTE);
		$analysis['SalesUfSecondLastYear']= (new SalesUfSecondLastYear)->get($CTE);
		$analysis['SalesThirdLastYear']= (new SalesThirdLastYear)->get($CTE);
		$analysis['SalesUfThirdLastYear']= (new SalesUfThirdLastYear)->get($CTE);
		$analysis['Last_vs_2Last']= (new LastYearSalesVsSecondLastYearSales)->get($CTE);
		$analysis['2Last_vs_3Last']= (new SecondLastYearSalesVsThirdLastYearSales)->get($CTE);
		$analysis['Charts']= (new BusinessChartsLoader)->get($CTE,$analysis);	


		return $analysis;
	}
}