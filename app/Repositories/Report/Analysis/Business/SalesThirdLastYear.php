<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
use acempresarial\Repositories\Report\Analysis\Business\AnnualSales;
class SalesThirdLastYear
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $years= (new AnnualSales)->get($CTE);
        $result = $years[2];
        return $result;
    }
}
