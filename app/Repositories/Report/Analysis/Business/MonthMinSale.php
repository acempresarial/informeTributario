<?php 
namespace acempresarial\Repositories\Report\Analysis\Business;


use acempresarial\Repositories\Report\Analysis\Business\SalesLast12MonthsArray;
use acempresarial\Repositories\Report\Analysis\Business\MinSale;
use acempresarial\Repositories\Report\Analysis\Business\Last12MonthsArray;
use acempresarial\Helpers\PHPhelpers;


class MonthMinSale 
{
	
	public function get($CTE)
	{
		return $this->recipe($CTE);
	}
	
	
	private function recipe($CTE)
	{	
		$sales = (new SalesLast12MonthsArray)->get($CTE);
		$minimum = (new MinSale)->get($CTE)['amount'];
		$months = (new Last12MonthsArray)->get($CTE);

		$i=0;
		$position = 0;
		foreach ($sales as $sale) {
			if($sale == $minimum)
			{
				$position = $i;
			}
			$i++;
		}
		
		if(isset($months[$position]))
		{
			$result = $months[$position]['date'];
			$helper = new PHPhelpers();
			$result = $helper->datetime_to_string($result);			
			return $result['strings']['month'];
		}
		
		
	}
	
}