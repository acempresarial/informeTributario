<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
use acempresarial\Repositories\Report\Analysis\Business\Last3Years;
class ThirdLastYear
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $years= (new Last3Years)->get($CTE);
         $result = [];
        if(isset($years[2]))
        {
            $result = $years[2];
        }
       
        return $result;
    }
}
