<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
use acempresarial\Repositories\Report\Analysis\Business\SalesLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesSecondLastYear;    
class LastYearSalesVsSecondLastYearSales
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $last= (new SalesLastYear)->get($CTE)['amount'];
        $secondlast = (new SalesSecondLastYear)->get($CTE)['amount'];
        if($secondlast != 0)
        {
            $value = ($last-$secondlast)/$secondlast;
            return $value;
        }
        
     
        
    }
}
