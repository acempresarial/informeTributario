<?php 
namespace acempresarial\Repositories\Report\Analysis\Business;


use acempresarial\Repositories\Report\Analysis\Business\MonthlySalesUf;

class TotalMensualSalesUf 
{
	
	public function get($CTE)
	{
		return $this->recipe($CTE);
	}
	
	
	private function recipe($CTE)
	{	
		$sales = (new MonthlySalesUf)->get($CTE);
		$suma = 0;
		foreach ($sales as $venta) {
			$suma = $suma + $venta;
		}
		return $suma;
	}
	
}