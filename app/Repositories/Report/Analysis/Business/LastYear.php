<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
use acempresarial\Repositories\Report\Analysis\Business\Last3Years;
class LastYear
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $years= (new Last3Years)->get($CTE);
        $result = $years[0];
        return $result;
    }
}
