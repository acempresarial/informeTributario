<?php
namespace acempresarial\Repositories\Report\Analysis\Business;


use acempresarial\Models\F29;
use acempresarial\Helpers\PHPhelpers;

class Last12MonthsArray
{
    private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }
    
    
    private function recipe()
    {
        $helper = new PHPhelpers();
        $id = $this->CTE->id;

        $datos = F29::where('cte_id', $id)->select('C15')->orderBy('C15', 'desc')
               ->take(12)
               ->get();
        
        $arreglo = $datos->toArray();
        $array = [];
        $i=0;
        foreach ($arreglo as $keys => $values) {
            foreach ($values as $key => $value) {                
                $array[$i] = $helper->datetime_to_string($value);
                $i++;
            }
        }      

        $result = $array;
        return $result;
    }
}
