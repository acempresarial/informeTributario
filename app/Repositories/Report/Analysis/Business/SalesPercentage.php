<?php 
namespace acempresarial\Repositories\Report\Analysis\Business;


use acempresarial\Repositories\Report\Analysis\Business\SalesLast12MonthsArray;
use acempresarial\Repositories\Report\Analysis\Business\TotalSalesLast12Months;

class SalesPercentage 
{
	private $CTE;
	
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$sales = (new SalesLast12MonthsArray)->get($this->CTE);
		$tot = (new TotalSalesLast12Months)->get($this->CTE);

		$arreglo = [];

		for ($w=0; $w < count($sales); $w++) { 

			if($tot == 0)
			{
				$arreglo[$w] = 0;
			}else{
				$arreglo[$w]=$sales[$w]/$tot;
			}
			
		}

		return $arreglo;
	}
	
}