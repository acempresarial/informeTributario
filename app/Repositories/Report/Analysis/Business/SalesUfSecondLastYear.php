<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
use acempresarial\Repositories\Report\Analysis\Business\SecondLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesSecondLastYear;
use DB;
class SalesUfSecondLastYear
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $year= (new SecondLastYear)->get($CTE);
        $sales = (new SalesSecondLastYear)->get($CTE)['amount'];
        
        $uff = DB::table('ufs')->where('year', $year->format('Y'))->orderBy('value','DESC')->first();//asumire que la uf sera del ultimo mes de ese año

        $UF = $uff->value;
        $valor = $sales/$UF;
        $result = $valor;
        return $result;
    }
}
