<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
use acempresarial\Repositories\Report\Analysis\Business\ThirdLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesThirdLastYear;
use DB;
class SalesUfThirdLastYear
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $year= (new ThirdLastYear)->get($CTE);
        $sales = (new SalesThirdLastYear)->get($CTE)['amount'];

        $uff = DB::table('ufs')->where('year', $year->format('Y'))->orderBy('value','DESC')->first();
        //asumire que la uf sera del ultimo mes de ese año

        $UF = $uff->value;
        $valor = $sales/$UF;        
        return $valor;
    }
}
