<?php 
namespace acempresarial\Repositories\Report\Analysis\Acceptance;
use acempresarial\Repositories\Report\Analysis\Acceptance\Checks\F22Existance;
use DB;
/**
* 
*/
class AcceptanceLoader 
{
	private $report;
	private $cte;
	public function get($cte ,$report)
	{
		$this->report = $report;
		$this->cte = $cte;			
		return $this->handle();
	}

	private function handle()
	{
		
		$acceptance['F22Exists']= (new F22Existance)->get($this->cte);	

		$acceptance['State'] = $this->checkAcceptance($acceptance);
		
			
		return $acceptance;	
		
	}

	private function checkAcceptance($acceptance)
	{
		foreach ($acceptance as $key => $check) {

			if($check['state']==false)
			{
				return false;
			}
			return true;
		}

	}
	
}