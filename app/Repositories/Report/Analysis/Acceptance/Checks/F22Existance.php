<?php
namespace acempresarial\Repositories\Report\Analysis\Acceptance\Checks;

class F22Existance
{
    
    private $cte;
    public function get($cte)
    {
        $this->cte = $cte;        
        return $this->check();
    }
    
    
    private function check()
    {

        $output = array();
        $output['state']=true;
        if($this->cte->f22s->count() == 0)
        {
            $output['state']=false;
            $output['reason']="Su carpeta tributaria no presenta formularios F22";           
        }                

         return $output;
    
        
    }
}