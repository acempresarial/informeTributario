<?php

namespace acempresarial\Repositories\Report\Analysis\BenchMark;
use acempresarial\Helpers\PHPhelpers;

class RemunerationsBenchMark
{
    private $Information;
    private $benchmark;
    public function get($Information, $benchmark)
    {
        $this->benchmark = $benchmark;
        $this->Information = $Information;
        return $this->recipe();
    }
    
    
    private function recipe()
    {
        $helper = new PHPhelpers();
        $benchmark = [];
        $company = $this->Information['report']['HHRR']['Remunerations']['Pesos'][0]['amount'];

        $benchmark['min'] = $helper->chart_millions_formatter($this->Information['UFfactor'] * $this->benchmark['min_remunerations']);
        $benchmark['max'] = $helper->chart_millions_formatter($this->Information['UFfactor'] * $this->benchmark['max_remunerations']) ;
        $benchmark['avg'] = $helper->chart_millions_formatter($this->Information['UFfactor'] * $this->benchmark['avg_remunerations']) ;
        $benchmark['stdv'] = $this->Information['UFfactor'] * $this->benchmark['stddev_remunerations'] ;

        $benchmark['company'] = $helper->chart_millions_formatter($company); 

         if($benchmark['company'] <= $benchmark['min'])
        {
            $text = "Las remuneraciones de su empresa están por debajo de las remuneraciones de la mayoría de las empresas de su mismo sector y tamaño.";
        }else if ($benchmark['company'] < $benchmark['avg'] * 0.8)
        {
         
            $text = "Su empresa tiene un costo en remuneraciones inferior al promedio de las empresas de su mismo sector y similar tamaño.";

        }else if ($benchmark['company'] >= $benchmark['avg'] * 0.8 && $benchmark['company'] < $benchmark['avg'] * 1.2)
        {
            $text = "Su empresa tiene un costo en remuneraciones similar  al promedio de las empresas de su mismo sector y similar tamaño.";
        }else if ($benchmark['company'] > $benchmark['avg'] * 1.2 && $benchmark['company'] < $benchmark['max'])
        {
            $text = "Su empresa tiene un costo en remuneraciones superior  al promedio de las empresas de su mismo sector y similar tamaño.";
        }else
        {
            $text = "Su empresa tiene un costo en remuneraciones superior al máximo del  de las empresas de su mismo sector y similar tamaño.";
        }
        $benchmark['text']=$text;              

        return $benchmark;
        
    }
}