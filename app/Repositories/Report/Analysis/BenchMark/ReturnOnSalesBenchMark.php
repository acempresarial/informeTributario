<?php

namespace acempresarial\Repositories\Report\Analysis\BenchMark;
use acempresarial\Helpers\PHPhelpers;

class ReturnOnSalesBenchMark
{
    private $Information;
    private $benchmark;
    public function get($Information, $benchmark)
    {
        $this->benchmark = $benchmark;
        $this->Information = $Information;
        return $this->recipe();
    }
    
    
    private function recipe()
    {
        $helper = new PHPhelpers();
        $benchmark = [];

        $company = array_reverse($this->Information['report']['Financial']['FinancialIndicators']['ReturnsOnSales'])[0]['amount'];

        $benchmark['min'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['min_rsv']) ;
        $benchmark['max'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['max_rsv']) ;
        $benchmark['avg'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['avg_rsv']) ;
        $benchmark['stdv'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['stddev_rsv']) ;

        $benchmark['company'] = $company;        
        
        return $benchmark;
        
    }
}