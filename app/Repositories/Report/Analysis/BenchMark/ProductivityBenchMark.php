<?php

namespace acempresarial\Repositories\Report\Analysis\BenchMark;

use acempresarial\Helpers\PHPhelpers;
class ProductivityBenchMark
{
    private $Information;
    private $benchmark;
    public function get($Information, $benchmark)
    {
        $this->benchmark = $benchmark;
        $this->Information = $Information;
        return $this->recipe();
    }
    
    /**
     * [recipe description]
     * @return [type] [description]
     */
    private function recipe()
    {
        $helper = new PHPhelpers();
        $benchmark = [];
        $F22 = $this->Information['cte']['F22s'][0];

        $company = 0;
        if($F22->C628 != 0)
        {
            $company = ($F22->C628 - $F22->C631) / $F22->C628;
        }       
        
        $benchmark['min'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['min_iremven']) ;
        $benchmark['max'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['max_iremven']) ;
        $benchmark['avg'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['avg_iremven']) ;
        $benchmark['stdv'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['stddev_iremven']) ;
        $benchmark['company'] = $helper->chart_porcentage_formatter($company); 
        
        if($benchmark['company'] <= $benchmark['min'])
        {
            $text = "¡Cuidado¡  Si se compara con empresas de su mismo sector y similar tamaño, su empresa está en el mínimo del indicador 'Productividad en ventas de las remuneraciones'. Lo que significa que en promedio está pagando mucho más en remuneraciones que sus competidores. Tiene un punto a su favor en la próxima negociación de salarios con sus trabajadores. Posiblemente necesita reorganizar a sus trabajadores.";

        }else if ($benchmark['company'] < $benchmark['avg'] * 0.8)
        {
         
            $text = "¡Cuidado¡ La 'Productividad en ventas de las remuneraciones'  de su empresa es bastante menor al promedio de las empresas de su mismo sector y similar tamaño. Lo que significa que en promedio está pagando más en remuneraciones que sus competidores. Tiene un punto a su favor en la próxima negociación de salarios con sus trabajadores. Puede mejorar su competitividad reorganizando a su fuerza de trabajo o basando las buenas relaciones laborales con sus trabajadores en otros aspectos adicionales al  salario. Posiblemente necesita reorganizar a sus trabajadores y/o obtener mayores ventas con el mismo equipo de trabajo.";

        }else if ($benchmark['company'] >= $benchmark['avg'] * 0.8 && $benchmark['company'] < $benchmark['avg'] * 1.2)
        {
            $text = "Su empresa está en el promedio de su sector de referencia para el indicador 'Productividad en ventas de las remuneraciones'. Si busca mejorar su competitividad debe pensar en alguna reorganización de sus trabajadores o en mejorar las ventas con el mismo equipo.";
        }else if ($benchmark['company'] > $benchmark['avg'] * 1.2 && $benchmark['company'] < $benchmark['max'])
        {
            $text = "La posición de su empresa en el indicador 'Productividad en ventas de las remuneraciones' señala que esta pagando en promedio remuneraciones mas bajas que la mayoría de las empresas de su competencia. Si su posición en los otros indicadores es igualmente positiva, puede seguir así, pero es posible que los próximos crecimientos en ventas deban ir acompañados por aumentos más que proporcionales de sus remuneraciones.";
        }else
        {
            $text = "Su empresa lidera en el indicador 'Productividad en ventas de las remuneraciones', lo que señala que o tiene una muy buena productividad de sus trabajadores o esta pagando en promedio las remuneraciones mas bajas de la competencia(empresas similares). Si su posición en los otros indicadores es igualmente positiva, puede seguir así. No obstante es posible que los próximos aumentos en ventas deban ir acompañados de aumentos más que proporcionales en las remuneraciones. Tenga claro que sus trabajadores podrían tener ofertas laborales con mejores condiciones.";
        }
        $benchmark['text']=$text;                    

        return $benchmark;
        
    }
}