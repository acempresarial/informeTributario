<?php

namespace acempresarial\Repositories\Report\Analysis\BenchMark;

use acempresarial\Helpers\PHPhelpers;
class SalesBenchMark
{
    private $Information;
    private $benchmark;
    public function get($Information, $benchmark)
    {
        $this->benchmark = $benchmark;
        $this->Information = $Information;
        return $this->recipe();
    }
    
    /**
     * [recipe description]
     * @return [type] [description]
     */
    private function recipe()
    {
        $helper = new PHPhelpers();
        $benchmark = [];
       
        $company = $this->Information['report']['Business']['SalesLastYear']['amount'];
       
        $benchmark['min'] = 
            $helper->chart_millions_formatter($this->Information['UFfactor'] * $this->benchmark['min_sells']) ;
        $benchmark['max'] = 
            $helper->chart_millions_formatter($this->Information['UFfactor'] * $this->benchmark['max_sells_3']) ;
        $benchmark['avg'] = 
            $helper->chart_millions_formatter($this->Information['UFfactor'] * $this->benchmark['avg_sells']) ;
        $benchmark['stdv'] = 
            $helper->chart_millions_formatter($this->Information['UFfactor'] * $this->benchmark['stddev_sells']) ;
        $benchmark['company'] = $helper->chart_millions_formatter($company);

        $text = "";

       

        if($benchmark['company'] <= $benchmark['min'])
        {
            $text = "Las ventas de su empresa están por debajo de las ventas de la mayoria de las empresas de su mismo sector y tamaño, por lo que tiene gran potencial de crecimiento en ventas. Aparentemente su empresa es muy poco competitiva en este momento.";
        }else if ($benchmark['company'] < $benchmark['avg'] * 0.8)
        {
         
            $text = "Su empresa está bajo la media de ventas de empresas de su mismo sector y similar tamaño, eso la hace menos competitiva que la mayoría de esas empresas.";

        }else if ($benchmark['company'] >= $benchmark['avg'] * 0.8 && $benchmark['company'] < $benchmark['avg'] * 1.2)
        {
            $text = "Su empresa está en el promedio de ventas de su sector de referencia. Para asegurar la posicion de su empresa en el mediano plazo debe hacer esfuerzos por mantener o aumentar las ventas.";
        }else if ($benchmark['company'] > $benchmark['avg'] * 1.2 && $benchmark['company'] < $benchmark['max'])
        {
            $text = "Su empresa esta sobre la media de ventas en su sector de referencia. Debe hacer esfuerzos por manetener esa buena posición competitiva; si es posible mejorarla.";
        }else
        {
            $text = "Su empresa lidera a su sector de referencia  en ventas. Felicitaciones. Cuide su posición de liderazgo.";
        }
        $benchmark['text']=$text;
        
       
        return $benchmark;
        
    }
}