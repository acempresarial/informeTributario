<?php 
namespace acempresarial\Repositories\Report\Analysis\GeneralInformation;

use DB;
/**
* El puntaje determina el segmento al que pertenece una 
* empresa, se utiliza el último formulario F22 cargado y 
* la UF del periodo anual correspondiente.
*/

class LastDate 
{
	private $cte;
	/**
	 * [get description]
	 * @param  [type] $CTE [description]
	 * @return [type]      [description]
	 */
	public function get($CTE)
	{
		$this->cte = $CTE;
		return $this->recipe();
	}
	
	
	/**
	 * [recipe description]
	 * @param  [type] $CTE [description]
	 * @return [type]      [description]
	 */
	private function recipe()
	{
		
		$last_date = [];
		
		$last_f22 = $this->cte->f22s->first();	

		$year = $last_f22->tax_year->format('Y');
		$month= $last_f22->tax_year->format('m');
		$day = $last_f22->tax_year->format('d');

		$last_date = 
			[
				'year'=>$year,
				'month'=>$month,
				'day'=>$day,
				'words'=>"$month del año $year"
			];


		return $last_date;
	}
	
}