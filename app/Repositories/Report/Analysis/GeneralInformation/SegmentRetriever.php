<?php 
namespace acempresarial\Repositories\Report\Analysis\GeneralInformation;

use acempresarial\Models\EconomicSector;
use DB;

/**
* Función que a partir del PUNTAJE calculado obtiene el SEGMENTO
*/
class SegmentRetriever 
{
	
	private $score;

	/**
	 * [get description]
	 * @param  [type] $score [description]
	 * @return [type]        [description]
	 */
	public function get($score)
	{
		$this->score = $score;
		return $this->handle();
	}

	/**
	 * [handle description]
	 * @return [type] [description]
	 */
	private function handle()
	{
		
		$segment = collect(DB::table('company_segment')->where('max_score','>=', $this->score)
		->where('min_score','<=',$this->score)->first())->toArray();

		return $segment;
	}
	
}
