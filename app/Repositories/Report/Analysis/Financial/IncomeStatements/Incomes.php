<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements;

use acempresarial\Helpers\PHPhelpers;
/**
* 
*/
class Incomes 
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
         $helper = new PHPhelpers();
    	$F22s = $this->CTE->f22s;
        $incomes = [];       

        foreach ($F22s  as $F22) {
            $calculation = $F22->C628 + $F22->C629;
            $incomes[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$calculation,
                    'chilean_curency_amount' => $helper->chilean_currency_formatter(($calculation)),
                    'millions_amount'=>$helper->millions_formatter($calculation),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($calculation)
                 ];        
           
        }
        $incomes = array_reverse($incomes);
        return $incomes;
    }
}