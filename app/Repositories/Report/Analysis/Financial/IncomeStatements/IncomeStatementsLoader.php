<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements;

use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\Incomes;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\GrossMargin;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\Costs;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\Administration_Sells_Expenses;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\OperationalResult;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\FinancialExpenses;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\Non_Operating_Income;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\GainsAndLossesOverPriceLevels;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\PreTaxProfit;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\RentTax;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\NetResult;
/**
* 
*/
class IncomeStatementsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the IncomeStatements page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($cte)
	{
	
		$analysis = array();
		$analysis['Incomes'] = (new Incomes)->get($cte);
		$analysis['Costs'] = (new Costs)->get($cte);
		$analysis['GrossMargin'] = (new GrossMargin)->get($cte);
		$analysis['Administration_Sells_Expenses'] = (new Administration_Sells_Expenses)->get($cte);
		$analysis['OperationalResult'] = (new OperationalResult)->get($cte);
		$analysis['FinancialExpenses'] = (new FinancialExpenses)->get($cte);
		$analysis['Non_Operating_Income'] = (new Non_Operating_Income)->get($cte);
		$analysis['GainsAndLossesOverPriceLevels'] = (new GainsAndLossesOverPriceLevels)->get($cte);
		$analysis['PreTaxProfit'] = (new PreTaxProfit)->get($cte);
		$analysis['RentTax'] = (new RentTax)->get($cte);
		$analysis['NetResult'] = (new NetResult)->get($cte);

		
		

		return $analysis;
	}
}