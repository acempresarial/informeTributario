<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements;

use acempresarial\Helpers\PHPhelpers;
/**
* 
*/
class FinancialExpenses
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    /**
     * [recipe description]
     * @return array [description]
     */
    private function recipe()
    {
        $helper = new PHPhelpers();
    	$F22s = $this->CTE->f22s;
        $expenses = [];       

        foreach ($F22s  as $F22) {

            $expenses[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=> $F22->C633,
                     'chilean_curency_amount' => $helper->chilean_currency_formatter(($F22->C633)),
                    'millions_amount'=>$helper->millions_formatter($F22->C633),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($F22->C633)
                 ];        
           
        }
        $expenses = array_reverse($expenses);
        return $expenses;
    }
}