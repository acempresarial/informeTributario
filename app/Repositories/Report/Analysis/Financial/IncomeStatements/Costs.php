<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements;

use acempresarial\Helpers\PHPhelpers;
/**
* 
*/
class Costs 
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
        $helper = new  PHPhelpers();
    	$F22s = $this->CTE->f22s;
        $costs = [];       

        foreach ($F22s  as $F22) {
         $calculation = $F22->C630 + $F22->C635;
            $costs[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$calculation,
                    'chilean_curency_amount' => $helper->chilean_currency_formatter(($calculation)),
                    'millions_amount'=>$helper->millions_formatter($calculation),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($calculation)
                 ];        
           
        }
        $costs = array_reverse($costs);
        return $costs;
    }
}