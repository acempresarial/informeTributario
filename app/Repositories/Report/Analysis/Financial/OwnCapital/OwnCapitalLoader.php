<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\OwnCapital;

use acempresarial\Repositories\Report\Analysis\Financial\OwnCapital\OwnCapital;
use acempresarial\Repositories\Report\Analysis\Financial\OwnCapital\WorkCapital;
use acempresarial\Repositories\Report\Analysis\Financial\OwnCapital\Capital_Assets_Relation;

/**
* 
*/
class OwnCapitalLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Financial analysis
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($cte)
	{
	
		$analysis = array();

    	$analysis['OwnCapital'] = (new OwnCapital)->get($cte);
    	$analysis['WorkCapital'] = (new WorkCapital)->get($cte);
    	$analysis['Capital_Assets_Relation'] = (new Capital_Assets_Relation)->get($cte);

    	
		return $analysis;
	}
}