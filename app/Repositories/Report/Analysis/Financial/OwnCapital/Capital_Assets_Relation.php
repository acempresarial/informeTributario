<?php 
namespace acempresarial\Repositories\Report\Analysis\Financial\OwnCapital;

/**
* 	
*/
class Capital_Assets_Relation 
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
        $F22s = $this->CTE->f22s;
        $relation = [];       

        foreach ($F22s  as $F22) {
            if($F22->C122 != 0 )
            {
                $relation[] = 
                     [
                        'year'=>$F22->tax_year->format('Y'),
                        'amount'=>($F22->C645 + $F22->C893 - $F22->C894)/$F22->C122
                     ]; 
            }                     
           
        }
       
        return $relation;
    }
}