<?php 
namespace acempresarial\Repositories\Report\Analysis\Financial\OwnCapital;

/**
* 	
*/
class WorkCapital 
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    /**
     * [recipe description]
     * @return [type] [description]
     */
    private function recipe()
    {
        $F22s = $this->CTE->f22s;
        $WorkCapital = [];       

        foreach ($F22s  as $F22) {
         
            $WorkCapital[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$F22->C645 + $F22->C893 - $F22->C894
                 ];          
           
        }
       
        return $WorkCapital;
    }
}