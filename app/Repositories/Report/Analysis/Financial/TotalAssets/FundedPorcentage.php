<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\TotalAssets;

/**
* 	
*/
class FundedPorcentage 
{
	
	private $analysis;
    public function get($analysis)
    {
        $this->analysis = $analysis;
        return $this->recipe();
    }

    /**
     * Calculates the Funded Porcentage
     * based on the previus analysis (Heritage and Assets)
     * @return [type] [description]
     */
    private function recipe()
    {    	
    	$AnnualAssets = $this->analysis['AnnualAssets'];
    	$Heritages = $this->analysis['Heritages'];

    	$FundedPorcentage = [];
    
    	for ($i=0; $i < count($Heritages); $i++) { 
    		
    		if($Heritages[$i]['amount'] != 0)
    		{
    				$FundedPorcentage[] = 
	    		[
	    			'year' => $Heritages[$i]['year'],
	    			'amount' =>  ($AnnualAssets['Pesos'][$i]['amount'] / $Heritages[$i]['amount'])/100
	    		];

    		}
    		
    		
    	}

    	return $FundedPorcentage;

    }
}

