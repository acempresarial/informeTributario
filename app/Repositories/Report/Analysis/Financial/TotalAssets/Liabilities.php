<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\TotalAssets;

use DB;
/**
* 
*/
class Liabilities 
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
    	$F22s = $this->CTE->f22s;
        $liabilities = [];       

        foreach ($F22s  as $F22) {
         
            $liabilities['Pesos'][] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$F22->C123
                 ];

            $uf = DB::table('ufs')->where('year', $F22->tax_year->format('Y'))->first();

            $liabilities['UF'][] = 
                [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=> ($F22->C123/$uf->value)
                ];
           
        }
    
        return $liabilities;
    }
}