<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\TotalAssets;

/**
* 		
*/
class Heritage
{
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
    	$F22s = $this->CTE->f22s;
        $heritage = []; 

        foreach ($F22s  as $F22) {
         
            $heritage[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$F22->C122 - $F22->C123
                 ];              
        }
    
        return $heritage;
    }
    
	
	
}
