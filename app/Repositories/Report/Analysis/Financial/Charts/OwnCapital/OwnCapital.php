<?php 
namespace acempresarial\Repositories\Report\Analysis\Financial\Charts\OwnCapital;

use acempresarial\Helpers\PHPhelpers;

class OwnCapital
{
	private $analysis;
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	

		$helper = new PHPhelpers();
		$result = [];
		
		$capitals = $this->analysis['OwnCapital']['OwnCapital'];
		
		foreach ($capitals as $capital) {
			
			$result[] =$helper->chart_millions_formatter($capital['amount']) ;

		}
		
		$result = array_reverse($result );
		
		$result = json_encode($result);
	
	    return $result;
	}
	
}