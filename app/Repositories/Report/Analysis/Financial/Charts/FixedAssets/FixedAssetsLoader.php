<?php 
namespace acempresarial\Repositories\Report\Analysis\Financial\Charts\FixedAssets;

use acempresarial\Repositories\Report\Analysis\Financial\Charts\FixedAssets\FixedAssets;

/**
* 
*/
class FixedAssetsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Business Charts section
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($analysis)
	{		
		$charts = array();			
		
		$charts['FixedAssets'] = (new FixedAssets)->get($analysis);
		
		return $charts;
	}
}