<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\Charts;

use acempresarial\Repositories\Report\Analysis\Financial\Charts\TotalAssets\TotalAssetsLoader;
use acempresarial\Repositories\Report\Analysis\Financial\Charts\FixedAssets\FixedAssetsLoader;
use acempresarial\Repositories\Report\Analysis\Financial\Charts\OwnCapital\OwnCapitalLoader;

/**
* 
*/
class FinancialChartsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Business Charts section
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($CTE,$analysis)
	{		
		$charts = array();	

		$charts['TotalAssets'] = (new TotalAssetsLoader)->get($analysis);
		$charts['FixedAssets'] = (new FixedAssetsLoader)->get($analysis);
		$charts['OwnCapital'] = (new OwnCapitalLoader)->get($analysis);
		
		
		return $charts;
	}
}