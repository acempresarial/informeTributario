<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\FinancialIndicators;

use acempresarial\Repositories\Report\Analysis\Financial\FinancialIndicators\ProfitOverEquity;
use acempresarial\Repositories\Report\Analysis\Financial\FinancialIndicators\AssetsRotation;
use acempresarial\Repositories\Report\Analysis\Financial\FinancialIndicators\ReturnsOnSales;
use acempresarial\Repositories\Report\Analysis\Financial\FinancialIndicators\ReturnsOnAssets;

/**
* 
*/
class FinancialIndicatorsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the FinancialIndicators page
	 * @param  [type] $cte               [description]
	 * @param  [type] $previousAnalysis  [description]
	 * @param  [type] $financialAnalysis [description]
	 * @return [type]                    [description]
	 */
	public function get($cte,$previousAnalysis, $financialAnalysis)
	{
	
		$analysis = array();
		$analysis['ProfitOverEquity'] = (new ProfitOverEquity)->get($cte);
		$analysis['AssetsRotation'] = (new AssetsRotation)->get($financialAnalysis,$previousAnalysis);
		$analysis['ReturnsOnSales'] = (new ReturnsOnSales)->get($cte);
		$analysis['ReturnsOnAssets'] = (new ReturnsOnAssets)->get($cte);
		
		
		return $analysis;
	}
}