<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\FinancialIndicators;
use acempresarial\Helpers\PHPhelpers;
/**
* 	
*/
class ReturnsOnAssets
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    /**
     * [recipe description]
     * @return [type] [description]
     */
    private function recipe()
    {

        $helper = new PHPhelpers();

        $F22s = $this->CTE->f22s;
        $returnOnAssets = [];       

        foreach ($F22s  as $F22) {
         
             if($F22->C628 != 0)
             {   
                $returnOnAssets[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$helper->chart_porcentage_formatter( ( ($F22->C628 + $F22->C629 + $F22->C651) - ($F22->C630 + $F22->C631 + $F22->C633) ) / ($F22->C122))
                 ];                   
             }
          
        }    
        $returnOnAssets = array_reverse($returnOnAssets);   
        return  $returnOnAssets;
    }
}