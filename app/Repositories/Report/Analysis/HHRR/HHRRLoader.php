<?php 

namespace acempresarial\Repositories\Report\Analysis\HHRR;

use acempresarial\Repositories\Report\Analysis\HHRR\Remunerations;
use acempresarial\Repositories\Report\Analysis\HHRR\RemunerationComparison;
use acempresarial\Repositories\Report\Analysis\HHRR\RemunerationsOverSales;
use acempresarial\Repositories\Report\Analysis\HHRR\Charts\HHRRChartsLoader;
/**
* 
*/
class HHRRLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the General information page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($cte)
	{
	
		$result = array();
		$result['Remunerations'] = (new Remunerations)->get($cte);
		$result['RemunerationsComparison']= (new RemunerationComparison)->get($result['Remunerations']);
		$result['RemunerationsOverSales']= (new RemunerationsOverSales)->get($cte);
		$result['Charts']= (new HHRRChartsLoader)->get($cte,$result);

		return $result;
	}
}