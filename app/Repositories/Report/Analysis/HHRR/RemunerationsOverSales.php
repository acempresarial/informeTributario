<?php 
namespace acempresarial\Repositories\Report\Analysis\HHRR;

use DB;
use acempresarial\Helpers\PHPhelpers;
/**
* 
*/

class RemunerationsOverSales
{
	private $CTE;	

	/**
	 * [get description]
	 * @param  [type] $CTE [description]
	 * @return [type]      [description]
	 */
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}

	/**
	 * Gets the Remunerations from the CTE
	 * @return [type] [description]
	 */
	public function recipe()
	{
		$helper = new PHPhelpers();
		$remunerations = [];
		foreach ($this->CTE->f22s as $F22) {
			
			if($F22->C628 != 0)
			{
					$porcentage =  (($F22->C631) / $F22->C628 );
					$remunerations[] = 
				[
					'year'=>$F22->tax_year->format('Y'),
					'amount'=>$helper->porcentage_formatter($porcentage )
				];	
			}
					
			
		}
		return $remunerations;
	}
	
}