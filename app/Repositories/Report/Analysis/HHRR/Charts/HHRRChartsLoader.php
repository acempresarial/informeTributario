<?php 

namespace acempresarial\Repositories\Report\Analysis\HHRR\Charts;

use acempresarial\Repositories\Report\Analysis\HHRR\Charts\Last3YearsRemunerations\Last3YearsRemunerationsLoader;

/**
* 
*/
class HHRRChartsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Business Charts section
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($CTE,$analysis)
	{		
		$charts = array();	

		$charts['RemunerationsLast3Years'] = (new Last3YearsRemunerationsLoader)->get($analysis);
		
		return $charts;
	}
}