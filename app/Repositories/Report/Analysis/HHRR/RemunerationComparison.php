<?php 

namespace acempresarial\Repositories\Report\Analysis\HHRR;

/**
* 		
*/
class RemunerationComparison
{
	private $Remunerations;	

	/**
	 * [get description]
	 * @param  [type] $CTE [description]
	 * @return [type]      [description]
	 */
	public function get($Remunerations)
	{
		$this->Remunerations = $Remunerations;
		return $this->recipe();
	}

	/**
	 * Compares the different years in their
	 * remuneration expenses
	 * @return [type] [description]
	 */
	public function recipe()
	{
		$comparison = [];
		$Remunerations = $this->Remunerations['Pesos'];

		if($Remunerations[1]['amount'] != 0 && $Remunerations[2]['amount'] != 0 )
		{
			$comparison['last_with_secondLast']      = ($Remunerations[0]['amount'] - $Remunerations[1]['amount'])/$Remunerations[1]['amount'];
			$comparison['secondLast_with_thirdLast'] = ($Remunerations[1]['amount'] - $Remunerations[2]['amount'])/$Remunerations[2]['amount'];
		}
		
		
		return $comparison;
	}
	
}
