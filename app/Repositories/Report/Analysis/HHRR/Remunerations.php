<?php 
namespace acempresarial\Repositories\Report\Analysis\HHRR;

use DB;
use acempresarial\Helpers\PHPhelpers;
/**
* 
*/

class Remunerations
{
	private $CTE;	

	/**
	 * [get description]
	 * @param  [type] $CTE [description]
	 * @return [type]      [description]
	 */
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}

	/**
	 * Gets the Remunerations from the CTE
	 * @return [type] [description]
	 */
	public function recipe()
	{
		$helper = new PHPhelpers();
		$remunerations = [];

		foreach ($this->CTE->f22s as $F22) {
			
			$remunerations['Pesos'][] = 
			[
				'year'=>$F22->tax_year->format('Y'),
				'amount'=>$F22->C631,
				'chilean_currency_amount'=>$helper->chilean_currency_formatter($F22->C631), 
				'millions_amount'=>$helper->millions_formatter($F22->C631),
				'chart_millions_amount'=>$helper->chart_millions_formatter($F22->C631)
			];

			$uf = DB::table('ufs')->where('year', $F22->tax_year->format('Y'))->first();

			$remunerations['UF'][] = 
			[
				'year'=>$F22->tax_year->format('Y'),
				'amount'=> ($F22->C631/$uf->value)
			];			

		}

		//Hay que exportar este calculo a su propia clase
		$porcentage = 0;
		$situation = "disminuyeron";

		foreach ($remunerations['Pesos'] as $key => $remuneration) {
			
			if(isset($remunerations['Pesos'][$key+1]) && $remunerations['Pesos'][$key+1]['amount'] != 0)
			{				
				$porcentage = ($remuneration['amount'] / $remunerations['Pesos'][$key+1]['amount']) -1 ;

				
				if($porcentage>0)
				{
					$situation = "aumentaron";
				}
				$remunerations['Pesos'][$key]['porcentage']=$helper->porcentage_formatter($porcentage) ;
				$remunerations['Pesos'][$key]['situation']=$situation ;
			}else{
				$remunerations['Pesos'][$key]['porcentage']=$helper->porcentage_formatter(0) ;
				$remunerations['Pesos'][$key]['situation']='Sin Ventas';				
			}
			
		}


		
		return $remunerations;
	}
	
}