<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations;


use acempresarial\Models\F29;
use acempresarial\Repositories\Report\Analysis\Operations\Last12Months;
use acempresarial\Repositories\Report\Analysis\Operations\PurchasesLast12MonthsArray;
use DB;

class MensualPurchasesUf 
{
	private $CTE;
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$months = (new Last12Months)->get($this->CTE);
		$purchases = (new PurchasesLast12MonthsArray)->get($this->CTE);
		$UFs = [];
		$UfPurchases = [];
		
		foreach ($months as $month) {
			$date = substr($month['date'], 0, 10);
			$uf = DB::table('ufs')->where('date', $date)->first();
			$UFs[] = $uf->value;				
		}		
		for ($q=0; $q < count($months) ; $q++) { 
			$UfPurchases[$q]=$purchases[$q]['amount']/$UFs[$q];
		}
		return $UfPurchases;
	}
	
}