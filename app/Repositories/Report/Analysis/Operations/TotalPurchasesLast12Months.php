<?php
namespace acempresarial\Repositories\Report\Analysis\Operations;

use acempresarial\Repositories\Report\Analysis\Operations\PurchasesLast12MonthsArray;
use acempresarial\Helpers\PHPhelpers;

class TotalPurchasesLast12Months
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    /**
     * [recipe description]
     * @param  [type] $CTE [description]
     * @return [type]      [description]
     */
    private function recipe($CTE)
    {
        $helper = new PHPhelpers();
        $purchases = (new PurchasesLast12MonthsArray)->get($CTE);
        $sum = 0;
        $totalPurchases = [];

        foreach ($purchases as $purchase) {
            $sum = $sum + $purchase['amount'];
        }

        $totalPurchases  = 
        [
            'amount' =>$sum,
            'chilean_currency'=>$helper->chilean_currency_formatter($sum),
            'millions_amount'=>$helper->millions_formatter($sum),
            'chart_millions_amount'=>$helper->chart_millions_formatter($sum)
        ];

        return $totalPurchases;
    }
}
