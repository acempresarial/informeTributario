<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations;


use acempresarial\Models\F29;
use acempresarial\Repositories\Report\Analysis\Business\Last12MonthsArray;
use acempresarial\Helpers\PHPhelpers;

class PurchasesLast12MonthsArray 
{
	private $CTE;
	
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$helper = new PHPhelpers();
		$id= $this->CTE->id;
		$dates = (new Last12MonthsArray)->get($this->CTE);

		$purchases = [];		
		foreach($dates as $date)
		{
			$F29 = F29::where('cte_id', $id)->where('C15',$date['date'])->first();

			$purchase = (($F29->C520  +  $F29->C514 +  $F29->C535 + $F29->C532 - $F29->C528 ) / 0.19 ) + ( ( $F29->C525 +  $F29->C553 ) / 0.19 );

			$purchases[] = 
				[
					'month'=>$date['numbers']['month'],
					'year'=>$date['numbers']['year'],
					'string_month'=>$date['strings']['month'],
					'amount'=>$purchase,
					'millions_amount'=>$helper->millions_formatter($purchase), 
					'char_millions_amount'=>$helper->chart_millions_formatter($purchase),
					'chilean_currency_amount'=> $helper->chilean_currency_formatter($purchase)
				];
			

		}          

		return $purchases;
	}
	
}