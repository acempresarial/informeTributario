<?php 

namespace acempresarial\Repositories\Report\Analysis\Operations\Charts;

use acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesLast12Months\PurchasesLast12MonthsLoader;
use acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesSellsDifference\PurchasesSellsDifferenceLoader;


/**
* 
*/
class OperationsChartsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Business Charts section
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($CTE,$analysis)
	{		
		$charts = array();		
		$charts['PurchasesLast12Month'] = (new PurchasesLast12MonthsLoader)->get($analysis);
		$charts['PurchasesSellsDifference'] = (new PurchasesSellsDifferenceLoader)->get($CTE,$analysis);
		
		

		
		return $charts;
	}
}