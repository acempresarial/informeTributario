<?php 

namespace acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesLast12Months;

use acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesLast12Months\Months;
use acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesLast12Months\Purchases;
use acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesLast12Months\Porcentages;
/**
* 
*/
class PurchasesLast12MonthsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the General information page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($analysis)
	{				
		$chart['labels'] = (new Months)->get($analysis);	
		$chart['purchases'] = (new Purchases)->get($analysis);	
		$chart['porcentages'] = (new Porcentages)->get($analysis);	
		return $chart;
	}
}