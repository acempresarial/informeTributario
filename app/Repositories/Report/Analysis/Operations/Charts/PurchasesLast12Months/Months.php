<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesLast12Months;


class Months
{
	private $analysis; 
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$result = [];
		
		$months = $this->analysis['PurchasesLast12Months'];
			
		foreach ($months as $month) {
			array_push($result,$month['string_month']);
		}	

		$result = array_reverse($result );
		
		$result = json_encode($result);
		
	    return $result;
	}
	
}