<?php 

namespace acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesSellsDifference;

use acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesLast12Months\Months;
use acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesSellsDifference\Difference;

/**
* 
*/
class PurchasesSellsDifferenceLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the General information page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($cte,$analysis)
	{				
		$chart['labels'] = (new Months)->get($analysis);	
		$chart['difference'] = (new Difference)->get($cte,$analysis);	
			
		return $chart;
	}
}