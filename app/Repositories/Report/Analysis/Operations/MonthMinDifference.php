<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations;


use acempresarial\Repositories\Report\Analysis\Operations\Last12Months;
use acempresarial\Repositories\Report\Analysis\Operations\DifferencesPurchasesSales;

class MonthMinDifference 
{
	private $CTE;	
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$meses = (new Last12Months)->get($this->CTE);
		$diferencia = (new DifferencesPurchasesSales)->get($this->CTE);
		$difabs= [];
		$monthCount = count($meses);

		for ($w=0; $w < $monthCount; $w++) { 
			$difabs[$w] =pow(pow($diferencia[$w],2), 0.5);
		}
		$colelctdiff = collect($difabs);
		$min = $colelctdiff->Min();

		$mes =[];
		for ($i=0; $i < $monthCount; $i++) { 
			if(abs($diferencia[$i])==abs($min))
			{
				$mes = $meses[$i];
			}
		}
	
		if($monthCount != 0)
		{
			return $mes;
		}
		
	}
	
}