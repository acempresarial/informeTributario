<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations;


use acempresarial\Repositories\Report\Analysis\Operations\PurchasesLast12MonthsArray;
use acempresarial\Repositories\Report\Analysis\Operations\TotalPurchasesLast12Months;
use acempresarial\Helpers\PHPhelpers;

class PercentageMonthlyPurchases 
{
	private $CTE;
	
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$helper = new PHPhelpers();
		$purchases = (new PurchasesLast12MonthsArray)->get($this->CTE);
		$totalPurchases = (new TotalPurchasesLast12Months)->get($this->CTE);
		$porcentages = [];
		for ($w=0; $w < count($purchases); $w++) { 
			
			$porcentage = 0;
			if($totalPurchases['amount'] != 0)
			{
				$porcentage = $purchases[$w]['amount']/$totalPurchases['amount'];
			}
			
			$porcentages[$w]= [			
				'porcentage' => $porcentage ,
				'formatted_porcentage'=> $helper->porcentage_formatter($porcentage),
				'char_formatted_porcentage'=>$helper->chart_porcentage_formatter($porcentage),
				'month' =>$purchases[$w]["month"],
				"string_month" =>$purchases[$w]["string_month"],
				'total' =>$totalPurchases['amount'],
				'porcentage_amount'=>$purchases[$w]				
			];
		}

		return $porcentages;
	}
	
}