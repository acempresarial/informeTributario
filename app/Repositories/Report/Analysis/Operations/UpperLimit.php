<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations;


use acempresarial\Repositories\Report\Analysis\Operations\AvgDifference;
use acempresarial\Repositories\Report\Analysis\Operations\DifferencesPurchasesSales;
use acempresarial\Helpers\PHPhelpers;
class UpperLimit 
{
	private $CTE;	
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	

		$helper = new PHPhelpers();
		$avg = (new AvgDifference)->get($this->CTE);
		$diferencia = (new DifferencesPurchasesSales)->get($this->CTE);
		$suma=0;
		foreach ($diferencia as $diff) {
			$a=($diff-$avg);
			$b=pow($a,2);
			$suma= $suma+$b;
		}
		$varianza=$suma/(count($diferencia)-1);
		$desvest=pow($varianza, 0.5);
		$limit=($avg+(1.96*$desvest));

		$limit=$helper->chart_millions_formatter($limit);
		return $limit;
	}
	
}