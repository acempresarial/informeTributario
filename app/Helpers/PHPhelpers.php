<?php
namespace acempresarial\Helpers;

use Carbon\Carbon;

class PHPhelpers
{
    /**
         * Extracs the value from the Xml when they
         * are an array.
         * @param  array or string $content
         * @return string
         */
      public function extract_value($content)
      {
          if (is_array($content)) {
              $content = $content['b'];
          }
          return $content;
      }

      /**
       * Takes the fields and orders its labels by length.
       * It is used to properly extract the PDF information.
       * @param  array $field [description]
       * @return array        [description]
       */
      public function order_fields_labels_by_length($fields)
      {
          foreach ($fields as $key => $field) {
              $lenghts = array();

              foreach ($field['labels'] as $number => $option) {
                  $lenghts[$number] = strlen($option['label']);
              }
              array_multisort($lenghts, SORT_DESC, $field['labels']);
              $fields[$key]['labels'] = $field['labels'];
          }
          return $fields;
      }

        /**
         * It compares if the string given stars with the
         * caracters from the $needle variable
         * @param  string $haystack [description]
         * @param  string $needle   [description]
         * @return bool           [description]
         */
        public function startsWith($haystack, $needle)
        {
           
         // search backwards starting from haystack length characters from the end
             return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
        }

        /**
        * Removes all spaces from the values of an array
        * @param array $F22 [description]
        */
        public function array_trim($array)
        {
            foreach ($array as $key => $value) {
                $array[$key] = trim($value);
            }

            return $array;
        }

        /**
         * Converts Chilean datetime to DB datetime string
         * @param [type] $date [description]
         */
        public function CHL_datetime_to_DB($date)
        {
            try {
                $formatted_date = Carbon::createFromFormat('d/m/Y H:i', $date)
                    ->toDateTimeString();
            } catch (Exception $e) {
                dd('Message: ' .$e->getMessage()) ;
            }
            return $formatted_date;
        }

        /**
         * Converts Chilean date with sepparated with -
         *  to DB datetime string (NOT IN USE; IS NOT WORKING)
         * @param $date [description]
         */
        public function CHL_date_to_DB($date)
        {
            $formatted_date = Carbon::createFromFormat('d-m-Y', '01−09−2008');
            return $formatted_date;
        }

         /**
         * Converts a given Month and Year to a DB datetime string         *
         * @param $date [description]
         */
        public function month_year_to_DB_datetime($month, $year)
        {
            $formatted_date = Carbon::createFromFormat('d-m-Y H:i', "01-$month-$year 00:00");
            return $formatted_date;
        }

         /**
         * Converts a given Datetime to string          *
         * @param $date [description]
         */
        public function datetime_to_string($datetime)
        {
            $date = [];
            setlocale(LC_TIME, 'es_ES.utf8');
            Carbon::setLocale('es');
            $formatted_date = Carbon::createFromFormat('Y-m-d H:i:s', $datetime);

          
            $date['date']= $formatted_date;
            $date['numbers']['day'] = $formatted_date->format('d');
            $date['numbers']['month'] = $formatted_date->format('m');
            $date['numbers']['year'] = $formatted_date->format('Y');

            $date['strings']['month'] =ucwords($formatted_date->formatLocalized('%B'));
            $date['strings']['day'] = ucwords($formatted_date->formatLocalized('%A'));
            $date['strings']['date'] = ucwords($formatted_date->formatLocalized('%A %d %B %Y'));
     
            return $date;
        }

       /**
        * Converts a given value to Chilean Millions
        * @param  [type] $number [description]
        * @return [type]         [description]
        */
        public function millions_formatter($number)
        {
           $formatted_number =  number_format($number / 1000000,2,',','.');

           return $formatted_number;
        }

        /**
        * Converts a given value to USA millons
        * @param  [type] $number [description]
        * @return [type]         [description]
        */
        public function chart_millions_formatter($number)
        {
           $formatted_number =  number_format($number / 1000000,2,'.','');

           return $formatted_number;
        }


        /**
         * Converts a given value to a TWO decimal porcentage
         * @param  [type] $number [description]
         * @return [type]         [description]
         */
        public function porcentage_formatter($number)
        {

          $formatted_number =  number_format($number * 100,2,',','.');
          
          return $formatted_number ;
        }

         /**
         * Converts a given value to a TWO decimal porcentage for Charts
         * @param  [type] $number [description]
         * @return [type]         [description]
         */
        public function chart_porcentage_formatter($number)
        {

          $formatted_number =  number_format($number * 100,2,'.','');
          
          return $formatted_number ;
        }

        /**
         * Converts a given value to a TWO decimal decimal for Charts
         * @param  [type] $number [description]
         * @return [type]         [description]
         */
        public function chart_decimal_formatter($number)
        {

          $formatted_number =  number_format($number,2,'.','');
          
          return $formatted_number ;
        }

        /**
         * Converts a given number into Chilean Currency
         * @param  [type] $amount [description]
         * @return [type]         [description]
         */
        public function chilean_currency_formatter($amount)
        {
          return number_format($amount,0,'.','.');
        }
}
