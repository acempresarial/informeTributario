<?php

namespace acempresarial\Http\Controllers;

use Illuminate\Http\Request;
use acempresarial\Http\Requests;
use acempresarial\Repositories\CTE\CteUploaderRepository;
use acempresarial\Repositories\CTE\CteSaverRepository;
use acempresarial\Models\Cte;
use Auth;
use acempresarial\Repositories\CTE\CteEvaluationRepository;
use acempresarial\Repositories\CTE\CteValidators\CteArrayValidator;
use DB;


class CTEsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    	
        $ctes = Cte::where('user_id','=',Auth::user()->id)
                ->orderBy('created_at','DESC')->get();

        $f29_count = 0;
        $f22_count = 0;
        foreach ($ctes as $cte) {
           
           $f29_count = $f29_count + $cte->f29s->count();
           $f22_count = $f22_count + $cte->f22s->count();
        }    

        return view('cte.index', compact('ctes','f29_count','f22_count'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {       
        $cte = Cte::findOrFail($id);
        return view('cte.show',compact('cte'));
    }

    /**
     * PDF upload for the CTE
     * @param  Request $request PDF
     * @param  int  $user_id User's ID
     * @return json           success/fail
     */
    public function upload(Request $request, $user_id,
    	CteUploaderRepository $uploadManager, CteSaverRepository $cte, CteArrayValidator $validator)
    {     
        $file = $request->file('file'); 
            
        $CTE = $uploadManager->upload($file,"uploads/ctes/$user_id/");

 
       // $validator->validate($CTE);

        $CTE = $cte->store($CTE);
     
        return 'Done';
    }

    /**
     * Shows the page where whe can Upload the
     * CTE's PDF
     * @return view
     */
    public function uploader()
    {
    	return view('cte.upload');
    }
    /**
     * Shows the Wizard to prepare the Report
     * @param [type] $id [description]
     */
    public function EconomicActivitiesWizard($id)
    {   
        $cte = CTE::findOrFail($id);
        return view('cte.wizard',compact('cte'));
    }
}
