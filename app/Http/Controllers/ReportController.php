<?php

namespace acempresarial\Http\Controllers;

use Illuminate\Http\Request;
use acempresarial\Repositories\Report\ReportBuilder;
use acempresarial\Models\Report;

use acempresarial\Http\Requests;

class ReportController extends Controller
{
	/**
	 * Shows all the reports generated for the CTE
	 * @param  [type] $cte [description]
	 * @return [type]      [description]
	 */
	public function index($cte)
	{

		$reports = Report::where('cte_id',$cte)->get();

		 return view('reports.index',compact('reports'));
	}

	/**
	 * Shows an specific report
	 * @param  [type] $cte [description]
	 * @return [type]      [description]
	 */
	public function show($id)
	{

		$report = Report::findOrFail($id);		
		return view('reports.show',compact('report'));
	}

	/**
	 * Generates the Report based on the information provided
	 * throut the wizard
	 * @param  Request       $request [description]
	 * @param  ReportBuilder $report  [description]
	 * @return [type]                 [description]
	 */
    public function generate(Request $request, ReportBuilder $report)
    {    	
    	$fields = $request->input('Report'); 
    	
    	$analysis = $report->generate($fields);

		if(isset($analysis['analysis']['state']) && $analysis['analysis']['state'] == false)
		{		
			$errors = collect($analysis['analysis']);
	
			/* It takes care of loading the error view when
     		/ the CTE has errors or when it can't be loaded
     		/ properly
     		*/
			return view('reports.error',compact('errors'));
			
		}

    	$analysis = json_encode($analysis);     	

    	$report = 
    	[
    		"name"=>$fields['name'],
    		"cte_id"=>$fields['cte_id'],
    		'data'=>$analysis
    	];
    	$report = Report::firstOrCreate($report);

    	return redirect()->action('ReportController@show', ['id' => $report->id]);
    	
    
    }

}
