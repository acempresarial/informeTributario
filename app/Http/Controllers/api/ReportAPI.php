<?php

namespace acempresarial\Http\Controllers\api;

use Illuminate\Http\Request;
use acempresarial\Models\Report;
use acempresarial\Http\Requests;
use acempresarial\Http\Controllers\Controller;
class ReportAPI extends Controller
{
	/**
	 * Shows all the reports generated for the CTE
	 * @param  [type] $cte [description]
	 * @return [type]      [description]
	 */
	public function index($cte)
	{

		$reports = Report::where('cte_id',$cte)->get();

		 return view('reports.index',compact('reports'));
	}

	/**
	 * Shows an specific report
	 * @param  [type] $cte [description]
	 * @return [type]      [description]
	 */
	public function show($id)
	{

		$report = Report::findOrFail($id);		
		 return $report;
	}

	/**
	 * Generates the Report based on the information provided
	 * throut the wizard
	 * @param  Request       $request [description]
	 * @param  ReportBuilder $report  [description]
	 * @return [type]                 [description]
	 */
    public function generate(Request $request, ReportBuilder $report)
    {    	
    	$fields = $request->input('Report'); 
    	$analysis = json_encode($report->generate($fields));     	

    	$report = 
    	[
    		"name"=>$fields['name'],
    		"cte_id"=>$fields['cte_id'],
    		'data'=>$analysis
    	];

    	$report = Report::firstOrCreate($report);


    	return redirect()->action('ReportController@show', ['id' => $report->id]);
    	
    
    }
}
