<?php

namespace acempresarial\Models;

use Illuminate\Database\Eloquent\Model;

class EconomicActivity extends Model
{

	protected $guarded = [
        'id'
    ];
     /**
     * Relation with the CTE 
     * @return [type] [description]
     */
    public function Ctes()
    {
        return $this->belongsToMany('acempresarial\Models\Cte')->withTimestamps();        
    }

     /**
     * Relation with the Economic Sector
     * @return [type] [description]
     */
    public function EconomicSector()
    {
        return $this->belongsTo('acempresarial\Models\EconomicSector');        
    }
}
