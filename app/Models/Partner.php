<?php

namespace acempresarial\Models;

use Illuminate\Database\Eloquent\Model;
use acempresarial\Helpers\PHPhelpers;

class Partner extends Model
{
     protected $guarded = [
        'id'
    ];

     public function cte()
    {
        return $this->belongsTo('acempresarial\Models\Cte');
    }

  
}
