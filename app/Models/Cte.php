<?php

namespace acempresarial\Models;

use Illuminate\Database\Eloquent\Model;
use acempresarial\Repositories\CTE\RelevantInformation;

class Cte extends Model
{
    protected $guarded = [
        'id'
    ];

    /**
     * Relation with the F22 form
     * @return [type] [description]
     */
    public function f22s()
    {
        return $this->hasMany('acempresarial\Models\F22');
    }

    /**
     * Relation with the F29 form
     * @return [type] [description]
     */
    public function f29s()
    {
        return $this->hasMany('acempresarial\Models\F29');
    }

      /**
     * Relation with the LegalRepresentatives model
     * @return [type] [description]
     */
    public function legalRepresentatives()
    {
        return $this->hasMany('acempresarial\Models\LegalRepresentative');
    }

    /**
     * Relation with the Partner model
     * @return [type] [description]
     */
    public function partners()
    {
        return $this->hasMany('acempresarial\Models\Partner');
    }

    /**
     * Relation with the Report model
     * @return [type] [description]
     */
    public function reports()
    {
        return $this->hasMany('acempresarial\Models\Report');
    }

     /**
     * Relation with the Economic Activities form
     * @return [type] [description]
     */
    public function economicActivities()
    {
        return $this->belongsToMany('acempresarial\Models\EconomicActivity')->withTimestamps();
    }

    /**
     * Relation with company.
     * @return [type] [description]
     */
    public function company()
    {
        return $this->belongsTo('acempresarial\Models\Company');
    }

    /**
     * Retreives all information relevant to the CTE
     */
    public function relevantInformation()
    {
        return RelevantInformation::get($this);
    }

     /**
     * Get all of the Economic sectors
     * of the CTE.
     */
    public function economicActivitiesArray()
    {
        $sectors = [];
        foreach ($this->economicActivities as $key => $activity) {
            $sectors[$activity->id]=$activity->name;     
        }
        return $sectors;
    }


}
