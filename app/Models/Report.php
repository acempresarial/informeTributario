<?php

namespace acempresarial\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $guarded = [
        'id'
    ];

     public function cte()
    {
        return $this->belongsTo('acempresarial\Models\Cte');
    }

    /**
     * Retreives the Json from the DB
     * and properly formats it.
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function getDataAttribute($value)
    {
    	return json_decode($value);
    }
}
